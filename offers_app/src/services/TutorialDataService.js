import http from "../http-common";

class TutorialDataService {
  getAll() {
    return http.get("/tutorials");
  }

  get(id) {
    return http.get(`/tutorials/${id}`);
  }

  create(data) {
    return http.post("/tutorials", data);
  }

  update(id, data) {
    return http.put(`/tutorials/${id}`, data);
  }

  delete(id) {
    return http.delete(`/tutorials/${id}`);
  }

  deleteAll() {
    return http.delete(`/tutorials`);
  }

  findByTitle(title) {
    return http.get(`/tutorials?title=${title}`);
  }

  getAllFav() {
    return http.get('/favourites');
  }

  deleteFav(id) {
    return http.delete(`/favourites/${id}`);
  }

  deleteAllFav() {
    return http.delete(`/favourites`);
  }
}

export default new TutorialDataService();
