import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      alias: "/offers",
      name: "offers",
      component: () => import("./components/OffersList")
    },
    {
      path: "/offers/:id",
      name: "offers-details",
      component: () => import("./components/Offer")
    },
    {
      path: "/add",
      name: "add",
      component: () => import("./components/AddOffer")
    },
    {
      path: "/marketplace",
      name: "marketplace",
      component: () => import("./components/Marketplace")
    }
  ]
});
