# Repte ABP

## Definició i anàlisis

### Qué farem?

Crearem una aplicació en grup d'una pàgina que mostrara ofertes de treball. Estarà formada per una secció on es mostraran les ofertes i un buscador per a poder filtrarles per titol.

A més, crearem una barra de navegació on incorporarem el Routing que ens permetira enllaçar cap altres components (Afegir oferta, Modificar oferta o Llistar ofertes).

Per treballar utilitzarem Vue-Cli per crear l'aplicació. L'aplicació es dividira en diferents fases, la cual es faran diferents rames amb GIT per tenir un control de les diferents seccions de l'aplicació.

En el cas de veure que hi hagi alguna funcionalitat que s'adecua a les nostres necessitats crearem una release per tenir una versió que podrem consultar en qualsevol moment.

[Link al git](https://gitlab.com/iaw47871833/repteabp)